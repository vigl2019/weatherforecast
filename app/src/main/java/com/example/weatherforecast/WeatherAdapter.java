package com.example.weatherforecast;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.weatherforecast.model.WeatherDB;
import com.example.weatherforecast.utils.ISharedPreferencesKeys;
import com.example.weatherforecast.utils.SharedPreferencesManager;

import java.util.List;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder> {

    private Context context;
    private List<WeatherDB> weatherDBList;
    private SharedPreferencesManager sharedPreferencesManager;

    TextView dtTxt;
    ImageView icon;
    TextView main;
    TextView description;
    TextView temp;

    private OnItemClickListener listener;

    interface OnItemClickListener {
        void onClick(WeatherDB weatherDB);
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    //================================================================================//

    public WeatherAdapter(Context context, List<WeatherDB> weatherDBList) {
        this.context = context;
        this.weatherDBList = weatherDBList;
    }

    @NonNull
    @Override
    public WeatherAdapter.WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        CardView cardView = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weather_cardview, parent, false);

        dtTxt = cardView.findViewById(R.id.ma_dt_txt);
        icon = cardView.findViewById(R.id.ma_icon);
        main = cardView.findViewById(R.id.ma_main);
        description = cardView.findViewById(R.id.ma_description);
        temp = cardView.findViewById(R.id.ma_temp);

        return new WeatherViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherAdapter.WeatherViewHolder holder, int position) {

        WeatherDB weather = weatherDBList.get(position);

        dtTxt.setText("Date: " + weather.getDtTxt());
        Glide.with(context).load(weather.getIconUrl()).into(icon);
        main.setText("Main: " + weather.getMain());
        description.setText("Description: " + weather.getDescription());

        //==================================================//

        sharedPreferencesManager = new SharedPreferencesManager(context);
        String temperatureScale = sharedPreferencesManager.getSharedPreference(ISharedPreferencesKeys.TEMPERATURE_SCALE, ISharedPreferencesKeys.DEFAULT_TEMPERATURE_SCALE);
        String temperatureScaleText = sharedPreferencesManager.getTemperatureScaleText(temperatureScale);

        temp.setText("Temperature: " + weather.getTemp() + " degrees " + temperatureScaleText);
    }

    @Override
    public int getItemCount() {
        return weatherDBList.size();
    }

    //================================================================================//

    class WeatherViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;

        public WeatherViewHolder(@NonNull CardView itemView) {
            super(itemView);

            cardView = itemView;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener != null) {
                        listener.onClick(weatherDBList.get(getAdapterPosition()));
                    }
                }
            });
        }
    }
}