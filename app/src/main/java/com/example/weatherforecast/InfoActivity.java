package com.example.weatherforecast;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.weatherforecast.model.WeatherDB;
import com.example.weatherforecast.utils.ISharedPreferencesKeys;
import com.example.weatherforecast.utils.SharedPreferencesManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InfoActivity extends AppCompatActivity {

    @BindView(R.id.ia_city_name)
    TextView cityName;

    @BindView(R.id.ia_dt_txt)
    TextView dtTxt;

    @BindView(R.id.ia_icon)
    ImageView icon;

    @BindView(R.id.ia_description)
    TextView description;

    @BindView(R.id.ia_temp)
    TextView temp;

    @BindView(R.id.ia_main)
    TextView main;

    @BindView(R.id.ia_pressure)
    TextView pressure;

    @BindView(R.id.ia_humidity)
    TextView humidity;

    @BindView(R.id.ia_wind_speed)
    TextView windSpeed;

    private SharedPreferencesManager sharedPreferencesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        ButterKnife.bind(this);

        WeatherDB weatherDB = getIntent().getParcelableExtra("WeatherDB");

        sharedPreferencesManager = new SharedPreferencesManager(InfoActivity.this);

        String cityNameText = sharedPreferencesManager.getSharedPreference(ISharedPreferencesKeys.CITY_NAME, ISharedPreferencesKeys.DEFAULT_CITY_NAME);
        cityName.setText(cityNameText);

        dtTxt.setText("Date: " + weatherDB.getDtTxt());
        Glide.with(this).load(weatherDB.getIconUrl()).into(icon);
        description.setText("Description: " + weatherDB.getDescription());

        //==================================================//

        String temperatureScale = sharedPreferencesManager.getSharedPreference(ISharedPreferencesKeys.TEMPERATURE_SCALE, ISharedPreferencesKeys.DEFAULT_TEMPERATURE_SCALE);
        String temperatureScaleText = sharedPreferencesManager.getTemperatureScaleText(temperatureScale);

        temp.setText("Temperature: " + weatherDB.getTemp() + " degrees " + temperatureScaleText);

        //==================================================//

        main.setText("Main: " + weatherDB.getMain());
        pressure.setText("Pressure: " + weatherDB.getPressure());
        humidity.setText("Humidity: " + weatherDB.getHumidity() + " %");
        windSpeed.setText("Wind speed: " + weatherDB.getWindSpeed() + " m/s");
    }
}

