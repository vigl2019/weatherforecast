package com.example.weatherforecast.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class WeatherDB implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String dtTxt;
    private String iconUrl;
    private String main;
    private String description;

//    private double temp;
//    private double pressure;
//    private double seaLevel;
//    private int humidity;
//    private double windSpeed;

    private String temp;
    private String pressure;
    private String seaLevel;
    private String humidity;
    private String windSpeed;

    public WeatherDB() {
    }

    public WeatherDB(String dtTxt, String iconUrl, String main, String description,
                     String temp, String pressure, String seaLevel, String humidity, String windSpeed) {
        this.dtTxt = dtTxt;
        this.iconUrl = iconUrl;
        this.main = main;
        this.description = description;
        this.temp = temp;
        this.pressure = pressure;
        this.seaLevel = seaLevel;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
    }

    /*
    public WeatherDB(String dtTxt, String iconUrl, String main, String description, double temp,
                     double pressure, double seaLevel, int humidity, double windSpeed) {
        this.dtTxt = dtTxt;
        this.iconUrl = iconUrl;
        this.main = main;
        this.description = description;
        this.temp = temp;
        this.pressure = pressure;
        this.seaLevel = seaLevel;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
*/
    //================================================================================//

    protected WeatherDB(Parcel in) {
        id = in.readInt();

        dtTxt = in.readString();
        iconUrl = in.readString();
        main = in.readString();
        description = in.readString();

        temp = in.readString();
        pressure = in.readString();
        seaLevel = in.readString();
        humidity = in.readString();
        windSpeed = in.readString();

//        temp = in.readDouble();
//        pressure = in.readDouble();
//        seaLevel = in.readDouble();
//        humidity = in.readInt();
//        windSpeed = in.readDouble();
    }

    public static final Creator<WeatherDB> CREATOR = new Creator<WeatherDB>() {
        @Override
        public WeatherDB createFromParcel(Parcel in) {
            return new WeatherDB(in);
        }

        @Override
        public WeatherDB[] newArray(int size) {
            return new WeatherDB[size];
        }
    };

    //================================================================================//

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDtTxt() {
        return dtTxt;
    }

    public void setDtTxt(String dtTxt) {
        this.dtTxt = dtTxt;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getSeaLevel() {
        return seaLevel;
    }

    public void setSeaLevel(String seaLevel) {
        this.seaLevel = seaLevel;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    /*
    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getSeaLevel() {
        return seaLevel;
    }

    public void setSeaLevel(double seaLevel) {
        this.seaLevel = seaLevel;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }
*/

    //================================================================================//

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);

        dest.writeString(dtTxt);
        dest.writeString(iconUrl);
        dest.writeString(main);
        dest.writeString(description);

        dest.writeString(temp);
        dest.writeString(pressure);
        dest.writeString(seaLevel);
        dest.writeString(humidity);
        dest.writeString(windSpeed);

/*
        dest.writeDouble(temp);
        dest.writeDouble(pressure);
        dest.writeDouble(seaLevel);
        dest.writeInt(humidity);
        dest.writeDouble(windSpeed);
*/
    }
}


