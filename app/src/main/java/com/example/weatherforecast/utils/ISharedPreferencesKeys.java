package com.example.weatherforecast.utils;

import java.util.GregorianCalendar;

public interface ISharedPreferencesKeys {
    String WEATHER_FORECAST_PREFERENCES = "weather_forecast_preferences";
    String CREATE_DATE = "date";
    long DEFAULT_CREATE_DATE = new GregorianCalendar().getTimeInMillis();
    String CITY_NAME = "city";
    String DEFAULT_CITY_NAME = "Kiev";
    String TEMPERATURE_SCALE = "temperature_scale";
    String DEFAULT_TEMPERATURE_SCALE = ITemperatureScale.KELVIN;

    long MILLISECONDS_IN_ONE_DAY = 86_400_000; // 24 hours = 86_400_000 milliseconds
}