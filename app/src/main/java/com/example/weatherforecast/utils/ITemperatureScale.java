package com.example.weatherforecast.utils;

public interface ITemperatureScale {
    String CELSIUS = "metric";
    String KELVIN = "Kelvin";
    String FAHRENHEIT = "imperial";
}
