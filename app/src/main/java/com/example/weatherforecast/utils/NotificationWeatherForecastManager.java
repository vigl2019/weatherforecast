package com.example.weatherforecast.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.example.weatherforecast.MainActivity;
import com.example.weatherforecast.R;

public class NotificationWeatherForecastManager {

    Context context = App.getInstance().getContext();
    String CHANNEL_ID = "1";
    int PENDING_INTENT_ID = 1;
    int NOTIFY_ID_1 = 1;
    private long[] vibrationPattern = {0, 100, 1000, 300, 200, 100, 500, 200, 100};

    public NotificationCompat.Builder getNotificationBuilder() {

        NotificationCompat.Builder builder = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "New Channel # 1", NotificationManager.IMPORTANCE_HIGH);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            channel.setShowBadge(true);
            channel.setVibrationPattern(vibrationPattern);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(context, CHANNEL_ID);
        } else {
            builder = new NotificationCompat.Builder(context);
        }

        return builder;
    }

    public void createNotification(String notificationTitle, String notificationText, NotificationCompat.Builder builder) {

        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, PENDING_INTENT_ID,
                                        notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setVibrate(vibrationPattern)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText);

        Notification notification = builder.build();
        notificationManager.notify(NOTIFY_ID_1, notification);
    }
}
