package com.example.weatherforecast.utils;

import android.content.Context;
import android.widget.Toast;

import com.example.weatherforecast.ApiService;
import com.example.weatherforecast.R;
import com.example.weatherforecast.database.WeatherForecastDatabase;
import com.example.weatherforecast.model.ResponseModel;
import com.example.weatherforecast.model.WeatherDB;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class WeatherForecastManager {

    private Context context = App.getInstance().getContext();
    private CompositeDisposable disposables = new CompositeDisposable();
    private WeatherForecastDatabase database = App.getInstance().getDatabase();

    public void getWeatherForecastFromServerByCityName(final IHelper iHelper, final String cityName, final String appId) {

        disposables.add(ApiService.getWeatherForecastByCityName(cityName, appId)

                .map(new Function<ResponseModel, List<WeatherDB>>() {
                    @Override
                    public List<WeatherDB> apply(ResponseModel responseModel) {
                        final List<WeatherDB> weatherDBlist = Converter.convert(responseModel);
                        return weatherDBlist;
                    }
                })

                .filter(new Predicate<List<WeatherDB>>() {
                    @Override
                    public boolean test(List<WeatherDB> weatherDBlist) throws Exception {

                        boolean isweatherDBlistHaveValues = ((weatherDBlist != null) && (!weatherDBlist.isEmpty()));

                        if (!isweatherDBlistHaveValues) {
                            Helper.createAlertDialog(context.getResources().getString(R.string.empty_weatherDB_list), context);
                        }

                        return isweatherDBlistHaveValues;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .subscribe(new Consumer<List<WeatherDB>>() {
                    @Override
                    public void accept(List<WeatherDB> weatherDBlist) throws Exception {
                        database.getWeatherForecastDao().updateAll(weatherDBlist);
                        iHelper.getWeatherForecast(weatherDBlist);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
//                      createAlertDialog(throwable.getMessage(), context);
                        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }));
    }

    public void getWeatherForecastFromServerByCityNameAndUnits(final IHelper iHelper, final String cityName, final String temperatureScale, final String appId) {

        disposables.add(ApiService.getWeatherForecastByCityNameAndUnits(cityName, temperatureScale, appId)

                .map(new Function<ResponseModel, List<WeatherDB>>() {
                    @Override
                    public List<WeatherDB> apply(ResponseModel responseModel) {
                        final List<WeatherDB> weatherDBlist = Converter.convert(responseModel);
                        return weatherDBlist;
                    }
                })

                .filter(new Predicate<List<WeatherDB>>() {
                    @Override
                    public boolean test(List<WeatherDB> weatherDBlist) throws Exception {

                        boolean isweatherDBlistHaveValues = ((weatherDBlist != null) && (!weatherDBlist.isEmpty()));

                        if (!isweatherDBlistHaveValues) {
                            Helper.createAlertDialog(context.getResources().getString(R.string.empty_weatherDB_list), context);
                        }

                        return isweatherDBlistHaveValues;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .subscribe(new Consumer<List<WeatherDB>>() {
                    @Override
                    public void accept(List<WeatherDB> weatherDBlist) throws Exception {
                        database.getWeatherForecastDao().updateAll(weatherDBlist);
                        iHelper.getWeatherForecast(weatherDBlist);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
//                      createAlertDialog(throwable.getMessage(), context);
                        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }));
    }

    //================================================================================//

    public void getWeatherForecastFromServerByCityId() {

        disposables.add(ApiService.getWeatherForecastByCityId(context.getString(R.string.city_id), context.getString(R.string.appid))

                .map(new Function<ResponseModel, List<WeatherDB>>() {
                    @Override
                    public List<WeatherDB> apply(ResponseModel responseModel) {
                        final List<WeatherDB> weatherDBlist = Converter.convert(responseModel);
                        return weatherDBlist;
                    }
                })

                .filter(new Predicate<List<WeatherDB>>() {
                    @Override
                    public boolean test(List<WeatherDB> weatherDBlist) throws Exception {

                        boolean isweatherDBlistHaveValues = ((weatherDBlist != null) && (!weatherDBlist.isEmpty()));

                        if (!isweatherDBlistHaveValues) {
                            Helper.createAlertDialog(context.getResources().getString(R.string.empty_weatherDB_list), context);
                        }

                        return isweatherDBlistHaveValues;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .subscribe(new Consumer<List<WeatherDB>>() {
                    @Override
                    public void accept(List<WeatherDB> weatherDBlist) throws Exception {
                        database.getWeatherForecastDao().updateAll(weatherDBlist);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
//                      createAlertDialog(throwable.getMessage(), context);
                        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }));
    }

    public void getWeatherForecastFromServerByCityId(final IHelper iHelper, final String temperatureScale) {
        disposables.add(ApiService.getWeatherForecastByCityId(context.getString(R.string.city_id), context.getString(R.string.appid))

                .map(new Function<ResponseModel, List<WeatherDB>>() {
                    @Override
                    public List<WeatherDB> apply(ResponseModel responseModel) {
                        final List<WeatherDB> weatherDBlist = Converter.convert(responseModel, temperatureScale);
                        return weatherDBlist;
                    }
                })

                .filter(new Predicate<List<WeatherDB>>() {
                    @Override
                    public boolean test(List<WeatherDB> weatherDBlist) throws Exception {

                        boolean isweatherDBlistHaveValues = ((weatherDBlist != null) && (!weatherDBlist.isEmpty()));

                        if (!isweatherDBlistHaveValues) {
                            Helper.createAlertDialog(context.getResources().getString(R.string.empty_weatherDB_list), context);
                        }

                        return isweatherDBlistHaveValues;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .subscribe(new Consumer<List<WeatherDB>>() {
                    @Override
                    public void accept(List<WeatherDB> weatherDBlist) throws Exception {
                        database.getWeatherForecastDao().updateAll(weatherDBlist);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
//                      createAlertDialog(throwable.getMessage(), context);
                        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }));
    }

    //================================================================================//

    public void getWeatherForecastFromDataBase(final IHelper iHelper){

        disposables.add(database.getWeatherForecastDao().getAll()

                .filter(new Predicate<List<WeatherDB>>() {
                    @Override
                    public boolean test(List<WeatherDB> weatherDBlist) throws Exception {

                        boolean isWeatherDBlistHaveValues = ((weatherDBlist != null) && (!weatherDBlist.isEmpty()));

                        if (!isWeatherDBlistHaveValues) {
                            Helper.createAlertDialog(context.getResources().getString(R.string.empty_weatherDB_list), context);
                        }

                        return isWeatherDBlistHaveValues;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .subscribe(new Consumer<List<WeatherDB>>() {
                    @Override
                    public void accept(List<WeatherDB> weatherDBlist) throws Exception {
                        iHelper.getWeatherForecast(weatherDBlist);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
//                      createAlertDialog(throwable.getMessage(), context);
                        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }));
    }

    //================================================================================//

    public void unSubscribe() {
        disposables.clear();
    }
}
