package com.example.weatherforecast.utils;

import android.content.Context;
import android.content.SharedPreferences;
import static android.content.Context.MODE_PRIVATE;

public class SharedPreferencesManager {

    private SharedPreferences weatherForecastSettings;

    public SharedPreferencesManager(Context context) {
        weatherForecastSettings = context.getSharedPreferences(ISharedPreferencesKeys.WEATHER_FORECAST_PREFERENCES, MODE_PRIVATE);
    }

    //================================================================================//

    public void initSharedPreferencesStorage() {

        SharedPreferences.Editor editor = weatherForecastSettings.edit();

        editor.putString(ISharedPreferencesKeys.CREATE_DATE, String.valueOf(ISharedPreferencesKeys.DEFAULT_CREATE_DATE));
        editor.putString(ISharedPreferencesKeys.CITY_NAME, ISharedPreferencesKeys.DEFAULT_CITY_NAME);
        editor.putString(ISharedPreferencesKeys.TEMPERATURE_SCALE, ISharedPreferencesKeys.DEFAULT_TEMPERATURE_SCALE);

        editor.apply();
    }

    //================================================================================//

    public String getSharedPreference(String key, String defaultValue) {

        String value = "";

        if (weatherForecastSettings.contains(key)) {
            value = weatherForecastSettings.getString(key, defaultValue);
        }

        return value;
    }

    public void setSharedPreference(String key, String value) {

        if (weatherForecastSettings.contains(key)) {
            SharedPreferences.Editor editor = weatherForecastSettings.edit();
            editor.putString(key, value);
            editor.apply();
        }
    }

    //================================================================================//

    public String getTemperatureScaleText(String temperatureScale) {

        String temperatureScaleText = ISharedPreferencesKeys.DEFAULT_TEMPERATURE_SCALE;

        switch (temperatureScale) {
            case ITemperatureScale.CELSIUS:
                temperatureScaleText = "Celsius";
                break;
            case ITemperatureScale.FAHRENHEIT:
                temperatureScaleText = "Fahrenheit";
                break;
            default:
                break;
        }

        return temperatureScaleText;
    }
}



