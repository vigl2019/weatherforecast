package com.example.weatherforecast.utils;

import android.app.Application;
import android.content.Context;

import androidx.room.Room;

import com.example.weatherforecast.database.WeatherForecastDatabase;

public class App extends Application {
    public static App instance;

    private WeatherForecastDatabase database;
    private Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        context = App.this;
        context.deleteDatabase("WeatherForecastDatabase");
        database = Room.databaseBuilder(this, WeatherForecastDatabase.class, "WeatherForecastDatabase")
                .allowMainThreadQueries()
                .build();
    }

    public static App getInstance() {
        return instance;
    }

    public WeatherForecastDatabase getDatabase() {
        return database;
    }

    public Context getContext() {
        return context;
    }
}
