package com.example.weatherforecast.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.weatherforecast.R;

import java.text.NumberFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Helper {

    private String appId = App.getInstance().getContext().getString(R.string.appid);
    private Context context;
    private WeatherForecastManager weatherForecastManager = new WeatherForecastManager();
    private SharedPreferencesManager sharedPreferencesManager;

    public Helper(Context context){
        this.context = context;
    }

    //================================================================================//

    public String initApp(IHelper iHelper){

        sharedPreferencesManager = new SharedPreferencesManager(context);
        String cityName = ISharedPreferencesKeys.DEFAULT_CITY_NAME;

        if(!sharedPreferencesManager.getSharedPreference(ISharedPreferencesKeys.CREATE_DATE, String.valueOf(ISharedPreferencesKeys.DEFAULT_CREATE_DATE)).isEmpty()){
            cityName = sharedPreferencesManager.getSharedPreference(ISharedPreferencesKeys.CITY_NAME, ISharedPreferencesKeys.DEFAULT_CITY_NAME);
            String temperatureScale = sharedPreferencesManager.getSharedPreference(ISharedPreferencesKeys.TEMPERATURE_SCALE, ISharedPreferencesKeys.DEFAULT_TEMPERATURE_SCALE);

//          textCityname.setText(cityName);

            long createDate = Long.parseLong(sharedPreferencesManager.getSharedPreference(ISharedPreferencesKeys.CREATE_DATE, String.valueOf(ISharedPreferencesKeys.DEFAULT_CREATE_DATE)));
            long currentDate = new GregorianCalendar().getTimeInMillis();

            if ((currentDate - createDate) < ISharedPreferencesKeys.MILLISECONDS_IN_ONE_DAY) {
                weatherForecastManager.getWeatherForecastFromDataBase(iHelper);
            } else {
                if (temperatureScale.equals(ITemperatureScale.KELVIN)) {
                    weatherForecastManager.getWeatherForecastFromServerByCityName(iHelper, cityName, appId);
                } else {
                    weatherForecastManager.getWeatherForecastFromServerByCityNameAndUnits(iHelper, cityName, temperatureScale, appId);
                }
            }
        }else{
            sharedPreferencesManager.initSharedPreferencesStorage();
            weatherForecastManager.getWeatherForecastFromServerByCityName(iHelper, ISharedPreferencesKeys.DEFAULT_CITY_NAME, appId);
//          textCityname.setText(ISharedPreferencesKeys.DEFAULT_CITY_NAME);
        }

        return cityName;
    }

    public static void createAlertDialog(String errorMassage, Context context) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle("Error!")
                .setMessage(errorMassage)
                .setCancelable(false)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public static void createAlertDialog(String errorMassage, final View view, Context context) {

        androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(context);
        dialogBuilder.setTitle("Error of input data!")
                .setMessage(errorMassage)
                .setCancelable(false)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();

                        if (view != null) {
                            view.requestFocus();
                        }
                    }
                });

        androidx.appcompat.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public void createSettingsAlertDialog(final IHelper iHelper) {

//        RadioGroup temperatureScale = (RadioGroup)findViewById(R.id.temperature_scale);
//        temperatureScale.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                switch (checkedId) {
//                    case R.id.celsius:
//                        helper = new Helper();
//                        helper.getWeather(iHelper, ITemperatureScale.CELSIUS);
//                        break;
//                    case R.id.kelvin:
//                        helper = new Helper();
//                        helper.getWeather(iHelper, ITemperatureScale.KELVIN);
//                        break;
//                    case R.id.fahrenheit:
//                        helper = new Helper();
//                        helper.getWeather(iHelper, ITemperatureScale.FAHRENHEIT);
//                        break;
//                    default:
//                        break;
//                }
//            }
//        });

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

//      LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.dialog_settings, null);
        dialogBuilder.setView(view);

        EditText cityNameEditText = view.findViewById(R.id.ds_edit_city_name);
        sharedPreferencesManager = new SharedPreferencesManager(context);
        cityNameEditText.setText(sharedPreferencesManager.getSharedPreference(ISharedPreferencesKeys.CITY_NAME, ISharedPreferencesKeys.DEFAULT_CITY_NAME));

        String temperatureText = sharedPreferencesManager.getSharedPreference(ISharedPreferencesKeys.TEMPERATURE_SCALE,
                                                                                ISharedPreferencesKeys.DEFAULT_TEMPERATURE_SCALE);
        RadioGroup temperatureScale = view.findViewById(R.id.ds_temperature_scale);

        switch (temperatureText) {
            case ITemperatureScale.CELSIUS:
                RadioButton rbtnCelsius = temperatureScale.findViewById(R.id.ds_celsius);
                rbtnCelsius.setChecked(true);
                break;
            case ITemperatureScale.KELVIN:
                RadioButton rbtnKelvin = temperatureScale.findViewById(R.id.ds_kelvin);
                rbtnKelvin.setChecked(true);
                break;
            case ITemperatureScale.FAHRENHEIT:
                RadioButton rbtnFahrenheit = temperatureScale.findViewById(R.id.ds_fahrenheit);
                rbtnFahrenheit.setChecked(true);
                break;
            default:
                break;
        }

        //================================================================================//

        dialogBuilder.setTitle("Weatherforecast Settings")
                .setCancelable(false)

                .setPositiveButton("Get weather forecast", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        EditText cityNameEditText = view.findViewById(R.id.ds_edit_city_name);
                        String cityName = cityNameEditText.getText().toString().trim();

                        if (cityName.isEmpty()) {
//                            TextView cityNameError = view.findViewById(R.id.ds_city_name_error);
//                            cityNameError.setText("Enter city name!");

                            cityName = ISharedPreferencesKeys.DEFAULT_CITY_NAME;
                        } else {

                        }

                        getWeatherForecast(iHelper, cityName);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void getWeatherForecast(IHelper iHelper, String cityName) {

        sharedPreferencesManager = new SharedPreferencesManager(context);
        sharedPreferencesManager.setSharedPreference(ISharedPreferencesKeys.CITY_NAME, cityName);

        String temperatureScale = sharedPreferencesManager.getSharedPreference(ISharedPreferencesKeys.TEMPERATURE_SCALE,
                                                                                ISharedPreferencesKeys.DEFAULT_TEMPERATURE_SCALE);

        if (temperatureScale.equals(ITemperatureScale.KELVIN)) {
            weatherForecastManager.getWeatherForecastFromServerByCityName(iHelper, cityName, appId);
        } else {
            weatherForecastManager.getWeatherForecastFromServerByCityNameAndUnits(iHelper, cityName, temperatureScale, appId);
        }
    }

    //================================================================================//

    public static String convertNumberToString(Number value) {
        Locale locale = Locale.ENGLISH;
//      NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
//      NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        numberFormat.setMinimumFractionDigits(2); // trailing zeros
        numberFormat.setMaximumFractionDigits(2); // round to 2 digits

        return numberFormat.format(value);
    }
}














