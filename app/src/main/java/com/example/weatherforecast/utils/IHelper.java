package com.example.weatherforecast.utils;

import com.example.weatherforecast.model.WeatherDB;

import java.io.Serializable;
import java.util.List;

public interface IHelper extends Serializable {
    void getWeatherForecast(List<WeatherDB> weatherDBList);
}