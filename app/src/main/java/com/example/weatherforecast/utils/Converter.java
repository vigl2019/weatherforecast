package com.example.weatherforecast.utils;

import android.content.Context;

import com.example.weatherforecast.R;
import com.example.weatherforecast.model.ResponseModel;
import com.example.weatherforecast.model.WeatherDB;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static List<WeatherDB> convert(ResponseModel responseModel) {

        List<WeatherDB> weatherDBList = new ArrayList<>();
        Context context = App.getInstance().getContext();
        String iconBaseUrl = context.getString(R.string.icon_base_url);
        String iconImageName = context.getString(R.string.icon_image_name);

        List<ResponseModel.List> responseModelList = responseModel.getList();

        for (int i = 0; i < responseModelList.size(); i++) {

            double tempDegreesKelvin = responseModel.getList().get(i).getMain().getTemp();

            WeatherDB weatherDB = new WeatherDB(responseModel.getList().get(i).getDtTxt(),
                    iconBaseUrl + responseModel.getList().get(i).getWeather().get(0).getIcon() + iconImageName,
                    responseModel.getList().get(i).getWeather().get(0).getMain(),
                    responseModel.getList().get(i).getWeather().get(0).getDescription(),

                    Helper.convertNumberToString(tempDegreesKelvin),
                    Helper.convertNumberToString(responseModel.getList().get(i).getMain().getPressure()),
                    Helper.convertNumberToString(responseModel.getList().get(0).getMain().getSeaLevel()),
                    Helper.convertNumberToString(responseModel.getList().get(i).getMain().getHumidity()),
                    Helper.convertNumberToString(responseModel.getList().get(0).getWind().getSpeed()));

            weatherDBList.add(weatherDB);
        }

        return weatherDBList;
    }

    public static List<WeatherDB> convert(ResponseModel responseModel, String temperatureScale) {

        List<WeatherDB> weatherDBList = new ArrayList<>();
        Context context = App.getInstance().getContext();
        String iconBaseUrl = context.getString(R.string.icon_base_url);
        String iconImageName = context.getString(R.string.icon_image_name);

        List<ResponseModel.List> responseModelList = responseModel.getList();

        for (int i = 0; i < responseModelList.size(); i++) {

            double tempDegreesKelvin = responseModel.getList().get(i).getMain().getTemp();
            double tempDegrees = 0.0d;

            switch(temperatureScale) {

                case ITemperatureScale.CELSIUS:
                    tempDegrees = tempDegreesKelvin - 273.15;
                    break;
                case ITemperatureScale.FAHRENHEIT:
                    tempDegrees = 1.8 * (tempDegreesKelvin - 273) + 32;
                    break;
                case ITemperatureScale.KELVIN:
                    tempDegrees = tempDegreesKelvin;
                default:
                    break;
            }

//          double tempDegreesFahrenheit = responseModel.getList().get(i).getMain().getTemp();
//          double tempDegreesCelsius = (tempDegreesFahrenheit - 32) * 5 / 9;

            WeatherDB weatherDB = new WeatherDB(responseModel.getList().get(i).getDtTxt(),
                    iconBaseUrl + responseModel.getList().get(i).getWeather().get(0).getIcon() + iconImageName,
                    responseModel.getList().get(i).getWeather().get(0).getMain(),
                    responseModel.getList().get(i).getWeather().get(0).getDescription(),

                    Helper.convertNumberToString(tempDegrees),
                    Helper.convertNumberToString(responseModel.getList().get(i).getMain().getPressure()),
                    Helper.convertNumberToString(responseModel.getList().get(0).getMain().getSeaLevel()),
                    Helper.convertNumberToString(responseModel.getList().get(i).getMain().getHumidity()),
                    Helper.convertNumberToString(responseModel.getList().get(0).getWind().getSpeed()));

            weatherDBList.add(weatherDB);
        }

        return weatherDBList;
    }
}