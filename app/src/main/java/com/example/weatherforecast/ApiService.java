package com.example.weatherforecast;

import com.example.weatherforecast.model.ResponseModel;

import io.reactivex.Flowable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ApiService {

    private static final String API = "http://api.openweathermap.org/data/2.5/";

    private static PrivateApi privateApi;

    public interface PrivateApi {

        @GET("forecast")
        Flowable<ResponseModel> getWeatherForecastByCityId(@Query("id") String cityId, @Query("APPID") String appId);

        @GET("forecast")
        Flowable<ResponseModel> getWeatherForecastByCityIdAndLang(@Query("id") String cityId, @Query("lang") String lang, @Query("APPID") String appId);

        //================================================================================//

        @GET("forecast")
        Flowable<ResponseModel> getWeatherForecastByCityName(@Query("q") String cityName, @Query("APPID") String appId);


        @GET("forecast")
        Flowable<ResponseModel> getWeatherForecastByCityNameAndUnits(@Query("q") String cityName, @Query("units") String units, @Query("APPID") String appId);

        //================================================================================//
/*
        // Weather forecast only on one day!
        @GET("find")
        Flowable<ResponseModel> getWeatherForecastByCityName(@Query("q") String cityName, @Query("APPID") String appId);

        // Weather forecast only on one day!
        @GET("find")
        Flowable<ResponseModel> getWeatherForecastByCityNameAndUnits(@Query("q") String cityName, @Query("units") String units, @Query("APPID") String appId);
*/
    }

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);
    }

    public static Flowable<ResponseModel> getWeatherForecastByCityId(String cityId, String appId){
        return privateApi.getWeatherForecastByCityId(cityId, appId);
    }

    public static Flowable<ResponseModel> getWeatherForecastByCityIdAndLang(String cityId, String lang, String appId){
        return privateApi.getWeatherForecastByCityIdAndLang(cityId, lang, appId);
    }

    //================================================================================//

    public static Flowable<ResponseModel> getWeatherForecastByCityName(String cityName, String appId){
        return privateApi.getWeatherForecastByCityName(cityName, appId);
    }

    public static Flowable<ResponseModel> getWeatherForecastByCityNameAndUnits(String cityName, String units, String appId){
        return privateApi.getWeatherForecastByCityNameAndUnits(cityName, units, appId);
    }

    //================================================================================//

/*
    public static Flowable<ResponseModel> getWeatherForecastByCityName(String cityName, String appId){
        return privateApi.getWeatherForecastByCityName(cityName, appId);
    }

    public static Flowable<ResponseModel> getWeatherForecastByCityName(String cityName, String units, String appId){
        return privateApi.getWeatherForecastByCityName(cityName, units, appId);
    }
*/
}

