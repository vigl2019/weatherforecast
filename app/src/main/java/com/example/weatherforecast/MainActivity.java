package com.example.weatherforecast;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherforecast.model.WeatherDB;
import com.example.weatherforecast.utils.Helper;
import com.example.weatherforecast.utils.IHelper;
import com.example.weatherforecast.utils.ISharedPreferencesKeys;
import com.example.weatherforecast.utils.ITemperatureScale;
import com.example.weatherforecast.utils.NotificationWeatherForecastManager;
import com.example.weatherforecast.utils.SharedPreferencesManager;
import com.example.weatherforecast.utils.WeatherForecastManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.am_city_name)
    TextView txtCityName;

    @BindView(R.id.settings_button)
    Button settingsButton;

    @BindView(R.id.update_button)
    Button updateButton;

    private Helper helper;
    private WeatherAdapter weatherAdapter;
    private WeatherForecastManager weatherForecastManager;
    private SharedPreferencesManager sharedPreferencesManager;

    private String appId;

    private IHelper iHelper = new IHelper() {
        @Override
        public void getWeatherForecast(List<WeatherDB> weatherDBList) {
            recyclerViewInit(weatherDBList);
        }
    };

    //================================================================================//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        appId = getString(R.string.appid);
        helper = new Helper(MainActivity.this);
        weatherForecastManager = new WeatherForecastManager();
        sharedPreferencesManager = new SharedPreferencesManager(MainActivity.this);

        helper.initApp(iHelper);

        //==================================================//

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helper.createSettingsAlertDialog(iHelper);
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferencesManager.setSharedPreference(ISharedPreferencesKeys.TEMPERATURE_SCALE, ITemperatureScale.KELVIN);
                String cityName = sharedPreferencesManager.getSharedPreference(ISharedPreferencesKeys.CITY_NAME, ISharedPreferencesKeys.DEFAULT_CITY_NAME);
                weatherForecastManager.getWeatherForecastFromServerByCityName(iHelper, cityName, appId);
            }
        });
    }

    //================================================================================//

    private void recyclerViewInit(List<WeatherDB> weatherDBList) {
        weatherAdapter = new WeatherAdapter(MainActivity.this, weatherDBList);
        recyclerView.setAdapter(weatherAdapter);

        weatherAdapter.setListener(new WeatherAdapter.OnItemClickListener() {
            @Override
            public void onClick(WeatherDB weatherDB) {
                sendIntent(weatherDB);
            }
        });

        //==================================================//

        String cityName = sharedPreferencesManager.getSharedPreference(ISharedPreferencesKeys.CITY_NAME, ISharedPreferencesKeys.DEFAULT_CITY_NAME);
        txtCityName.setText(cityName);

        String temperatureScale = sharedPreferencesManager.getSharedPreference(ISharedPreferencesKeys.TEMPERATURE_SCALE, ISharedPreferencesKeys.DEFAULT_TEMPERATURE_SCALE);
        String temperatureScaleText = sharedPreferencesManager.getTemperatureScaleText(temperatureScale);

        String notificationText = "City: " + cityName +
                "; " + "Current temperature: " + weatherDBList.get(0).getTemp() + " degrees " + temperatureScaleText;

        NotificationWeatherForecastManager notificationWeatherForecastManager = new NotificationWeatherForecastManager();
        NotificationCompat.Builder builder = notificationWeatherForecastManager.getNotificationBuilder();
        notificationWeatherForecastManager.createNotification("Weather forecast", notificationText, builder);
    }

    private void sendIntent(WeatherDB weatherDB) {
        Intent intent = new Intent(this, InfoActivity.class);
        intent.putExtra("WeatherDB", weatherDB);
        startActivity(intent);
    }

    //================================================================================//

    public void onRadioButtonClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.ds_celsius:
                if (checked) {
                    sharedPreferencesManager.setSharedPreference(ISharedPreferencesKeys.TEMPERATURE_SCALE, ITemperatureScale.CELSIUS);
                }
                break;
            case R.id.ds_kelvin:
                if (checked) {
                    sharedPreferencesManager.setSharedPreference(ISharedPreferencesKeys.TEMPERATURE_SCALE, ITemperatureScale.KELVIN);
                }
                break;
            case R.id.ds_fahrenheit:
                if (checked) {
                    sharedPreferencesManager.setSharedPreference(ISharedPreferencesKeys.TEMPERATURE_SCALE, ITemperatureScale.FAHRENHEIT);
                }
                break;
            default:
                break;
        }
    }

    //================================================================================//

    @Override
    public void onStop() {
        super.onStop();

        weatherForecastManager.unSubscribe();
    }
}