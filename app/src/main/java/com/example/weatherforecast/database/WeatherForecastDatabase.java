package com.example.weatherforecast.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import com.example.weatherforecast.model.WeatherDB;

@Database(entities= WeatherDB.class, version=1)
public abstract class WeatherForecastDatabase extends RoomDatabase {
    public abstract WeatherForecastDao getWeatherForecastDao();
}