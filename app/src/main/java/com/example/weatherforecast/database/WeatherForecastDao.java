package com.example.weatherforecast.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.weatherforecast.model.WeatherDB;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class WeatherForecastDao {

    @Insert
    public abstract void insertWeatherDB(List<WeatherDB> weatherDBList);

    @Query("SELECT * FROM WeatherDB")
    public abstract Flowable<List<WeatherDB>> getAll();

    @Query("SELECT last_insert_rowid()")
    public abstract int getLastId();

    @Query("DELETE FROM WeatherDB")
    public abstract void removeAll();

    public void updateAll(List<WeatherDB> weatherDBList) {
        removeAll();
        insertWeatherDB(weatherDBList);
    }
}


